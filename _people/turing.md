---
layout: post
title: Alan Turing
permalink: /people/turing
img: "assets/img/faces/turing.jpeg"
tags: [people, computer science, logic, cryptanalysis, philosophy, biology]
---

![Inside post photo](/assets/img/faces/turing.jpeg)

Alan Mathison Turing, OBE FRS (23 June 1912 -- 7 June 1954) was an
English mathematician, computer scientist, logician, cryptanalyst,
philosopher and theoretical biologist. Turing was highly influential
in the development of theoretical computer science, providing a
formalisation of the concepts of algorithm and computation with the
Turing machine, which can be considered a model of a general-purpose
computer. Turing is widely considered to be the father of theoretical
computer science and artificial intelligence. Despite these
accomplishments, he was never fully recognised in his home country
during his lifetime, due to his homosexuality, which was then a crime
in the UK, and because his work was covered by the Official Secrets Act.

Source:
<a href="https://en.wikipedia.org/wiki/Alan_Turing" title="Wikipedia articile on Alan Turing">Wikipedia</a>
