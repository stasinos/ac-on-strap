---
layout: post
title: Alonzo Church
permalink: /people/church
img: "assets/img/faces/church.jpeg"
tags: [people, computer science, logic, philosophy]
---

![Inside post photo](/assets/img/faces/church.jpeg)

Alonzo Church (June 14, 1903 -- August 11, 1995) was an American
mathematician and logician who made major contributions to
mathematical logic and the foundations of theoretical computer
science. He is best known for lambda calculus, the Church-Turing
thesis, proving the undecidability of the Entscheidungsproblem,
the Frege-Church ontology, and the Church–Rosser theorem. He also
worked on philosophy of language.

Source:
<a href="https://en.wikipedia.org/wiki/Alonzo_Church" title="Wikipedia articile on Alonzo Church">Wikipedia</a>
